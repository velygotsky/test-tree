<?php

namespace App;

use App\DataStructure\Tree\GenericTree;
use App\Algorithm\Sort\MergeSort;
use App\DataStructure\LinkedList\SinglyLinkedList;
use App\DataStructure\LinkedList\Interfaces\ListInterface;

/**
 * TaskTree class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
class TaskTree extends GenericTree
{
    /**
     * Reorganizes the leaves of the tree by sum of weights.
     *
     * @param int $weight The max weight of the node.
     */
    public function reorganize(int $weight)
    {
        $mergeSort = new MergeSort();
        $forNextNode = new SinglyLinkedList();
        foreach ($this->all() as $node) {
            // Attach the list of leaves from previous step.
            if ($forNextNode->count() > 0 && $node->getValue() === null) {
                $node->setValue($forNextNode);
            } elseif ($forNextNode->count() > 0) {
                $node->setValue($node->getValue()->merge($forNextNode));
            }

            if ($node->getValue() === null) {
                continue;
            }

            // Sort the linked list of leaves.
            $sorted = $mergeSort->sort($node->getValue());
            // Calculate split index by sum of the weights.
            $splitIndex = $this->calculateSplitIndex($sorted, $weight);

            // Save the newly organized list of leaves.
            $forCurrNode = $sorted->sublist(0, $splitIndex);
            $node->setValue($forCurrNode->count() > 0 ? $forCurrNode : null);

            // Move the extra leaves to the next tree node.
            $forNextNode = $sorted->sublist($splitIndex);
        }
    }

    /**
     * Calculates index of splitting for the list of leaves.
     *
     * @param ListInterface $sorted The sorted linked list.
     * @param type          $weight The max weight of the tree node.
     *
     * @return int The index of splitting.
     */
    private function calculateSplitIndex(ListInterface $sorted, int $weight): int
    {
        $sum = 0;
        foreach ($sorted as $key => $value) {
            $sum += $value;
            if ($sum > $weight) {
                return $key == 0 ? -1 : $key;
            } elseif ($sum == $weight) {
                return $key + 1;
            }
        }

        return $key + 1;
    }
}
