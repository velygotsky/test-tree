<?php

namespace App\DataStructure\LinkedList\Interfaces;

/**
 * ListInterface.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
interface ListInterface extends \Iterator, \Countable
{
    /**
     * Pops a node from the end of the linked list.
     *
     * @return mixed The value of the popped node.
     */
    public function pop();

    /**
     * Pushes an element at the end of the linked list.
     *
     * @param mixed $value The value to push.
     */
    public function push($value);

    /**
     * Shifts a node from the beginning of the linked list.
     *
     * @return mixed The value of the shifted node.
     */
    public function shift();

    /**
     * Prepends the linked list with an element.
     *
     * @param mixed $value The value to unshift.
     */
    public function unshift($value);

    /**
     * Returns the sub-list of the list.
     *
     * @param int      $offset The offset.
     * @param int|null $length The length of the sub-list.
     *
     * @return ListInterface The sub-list of the list.
     */
    public function sublist(int $offset, ?int $length = null): ListInterface;

    /**
     * Merges values of the list to the current list.
     *
     * @param ListInterface $list The list to attach.
     */
    public function merge(ListInterface $list);
}
