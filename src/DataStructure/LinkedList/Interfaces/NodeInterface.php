<?php

namespace App\DataStructure\LinkedList\Interfaces;

/**
 * NodeInterface.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
interface NodeInterface
{
    /**
     * Constructor.
     *
     * @param mixed $value The value of the node.
     */
    public function __construct($value);

    /**
     * Gets the node value.
     */
    public function getValue();

    /**
     * Gets the next node relative to the current node.
     *
     * @return NodeInterface|null The next node relative to the current node.
     */
    public function getNext(): ?NodeInterface;

    /**
     * Sets the next node relative to the current node.
     *
     * @param NodeInterface|null $next The next node relative to the current node.
     */
    public function setNext(?NodeInterface $next);
}
