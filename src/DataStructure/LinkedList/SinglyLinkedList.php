<?php

namespace App\DataStructure\LinkedList;

use App\DataStructure\LinkedList\ListNode;
use App\DataStructure\LinkedList\Interfaces\ListInterface;
use App\DataStructure\LinkedList\Interfaces\NodeInterface;

/**
 * SinglyLinkedList class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
class SinglyLinkedList implements ListInterface
{
    /**
     * @var int
     */
    private $count;

    /**
     * @var NodeInterface|null
     */
    private $currentNode;

    /**
     * @var NodeInterface|null
     */
    private $firstNode;

    /**
     * @var NodeInterface|null
     */
    private $lastNode;

    /**
     * @var int
     */
    private $key;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->count = 0;
        $this->currentNode = null;
        $this->firstNode = null;
        $this->lastNode = null;
        $this->key = 0;
    }

    /**
     * Gets the current singly linked list node.
     *
     * @return mixed The current singly linked list node.
     */
    public function current()
    {
        return $this->currentNode !== null ? $this->currentNode->getValue() : null;
    }

    /**
     * Returns the current node index.
     *
     * @return int The current singly linked list node index.
     */
    public function key(): int
    {
        return $this->key;
    }

    /**
     * Moves the iterator to the next node.
     */
    public function next(): void
    {
        $this->key++;
        if ($this->currentNode !== null) {
            $this->currentNode = $this->currentNode->getNext();
        }
    }

    /**
     * Rewinds back to the first element of the Iterator.
     */
    public function rewind(): void
    {
        $this->key = 0;
        $this->currentNode = $this->firstNode;
    }

    /**
     * Checks if the singly linked list contains any more nodes.
     *
     * @return bool Returns TRUE if the singly linked list contains
     *  any more nodes, FALSE otherwise.
     */
    public function valid(): bool
    {
        return $this->key < $this->count;
    }

    /**
     * Counts the number of elements in the singly linked list.
     *
     * @return int The number of elements in the singly linked list.
     */
    public function count(): int
    {
        return $this->count;
    }

    /**
     * {@inheritdoc}
     */
    public function push($value)
    {
        $node = new ListNode($value);
        if ($this->lastNode !== null) {
            $this->lastNode->setNext($node);
        }
        if ($this->firstNode === null) {
            $this->firstNode = $node;
        }

        $this->lastNode = $node;
        $this->count++;
    }

    /**
     * {@inheritdoc}
     */
    public function shift()
    {
        if ($this->firstNode === null) {
            return null;
        }

        $value = $this->firstNode->getValue();
        $this->firstNode = $this->firstNode->getNext();
        $this->count--;
        if ($this->firstNode === null) {
            $this->lastNode = null;
            $this->key = 0;
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function pop()
    {
        if ($this->firstNode === null) {
            return null;
        }

        if ($this->firstNode->getNext() === null) {
            $value = $this->firstNode->getValue();
            $this->firstNode = null;
            $this->lastNode = null;
            $this->count = 0;
            $this->key = 0;

            return $value;
        }

        $previousNode = $this->firstNode;
        $currentNode = $this->firstNode->getNext();

        while ($currentNode->getNext() !== null) {
            $previousNode = $currentNode;
            $currentNode = $currentNode->getNext();
        }

        $value = $previousNode->getNext()->getValue();
        $previousNode->setNext(null);
        $this->lastNode = $previousNode;
        $this->count--;

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function unshift($value)
    {
        $node = new ListNode($value);
        $node->setNext($this->firstNode);
        $this->firstNode = $node;

        if ($this->lastNode === null) {
            $this->lastNode = $node;
        }

        $this->count++;
    }

    /**
     * {@inheritdoc}
     */
    public function sublist(int $offset, ?int $length = null): ListInterface
    {
        if ($length === null) {
            $length = $this->count;
        }

        $sublist = new SinglyLinkedList();
        if ($offset < 0 || $length < 0) {
            return $sublist;
        }

        foreach ($this as $key => $value) {
            if ($key < $offset) {
                continue;
            }

            $sublist->push($value);
            if ($sublist->count() == $length) {
                break;
            }
        }

        return $sublist;
    }

    /**
     * {@inheritdoc}
     */
    public function merge(ListInterface $list)
    {
        $merged = new SinglyLinkedList();
        foreach ($this as $value) {
            $merged->push($value);
        }
        foreach ($list as $value) {
            $merged->push($value);
        }

        return $merged;
    }

    /**
     * Creates singly linked list from array.
     *
     * @param array $values The array of values.
     *
     * @return SinglyLinkedList The SinglyLinkedList instance with values.
     */
    public static function fromArray(array $values): SinglyLinkedList
    {
        $list = new SinglyLinkedList();
        foreach ($values as $value) {
            $list->push($value);
        }

        return $list;
    }

    /**
     * Converts singly linked list to string.
     *
     * @return string The singly linked list converted to string.
     */
    public function __toString(): string
    {
        $arr = [];
        foreach ($this as $value) {
            $arr[] = $value;
        }

        return sprintf('[%s]', implode(', ', $arr));
    }
}
