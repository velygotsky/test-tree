<?php

namespace App\DataStructure\LinkedList;

use App\DataStructure\LinkedList\Interfaces\NodeInterface;

/**
 * ListNode class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
class ListNode implements NodeInterface
{
    /**
     * @var mixed
     */
    private $value;

    /**
     * @var NodeInterface
     */
    private $next;

    /**
     * {@inheritdoc}
     */
    public function __construct($value)
    {
        $this->value = $value;
        $this->next = null;
    }

    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function getNext(): ?NodeInterface
    {
        return $this->next;
    }

    /**
     * {@inheritdoc}
     */
    public function setNext(?NodeInterface $next): void
    {
        $this->next = $next;
    }
}
