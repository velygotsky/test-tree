<?php

namespace App\DataStructure\Tree\Interfaces;

use App\DataStructure\Tree\Interfaces\NodeInterface;

/**
 * ExporterInterface.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
interface ExporterInterface
{
    /**
     * Exports a tree into something.
     *
     * @param NodeInterface $node The node.
     *
     * @return mixed The exported tree.
     */
    public function export(NodeInterface $node);
}
