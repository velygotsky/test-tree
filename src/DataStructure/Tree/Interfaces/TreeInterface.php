<?php

namespace App\DataStructure\Tree\Interfaces;

use App\Algorithm\Traverser\Interfaces\TraverserInterface;
use App\DataStructure\Tree\Interfaces\ExporterInterface;
use App\DataStructure\Tree\Interfaces\NodeInterface;

/**
 * TreeInterface.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
interface TreeInterface
{
    /**
     * Constructor.
     *
     * @param TraverserInterface $traverser The traverser.
     * @param ExporterInterface  $exporter  The exporter.
     */
    public function __construct(
        TraverserInterface $traverser = null,
        ExporterInterface $exporter = null
    );

    /**
     * Gets the root of the node.
     *
     * @return NodeInterface|null The root node or NULL.
     */
    public function getRoot(): ?NodeInterface;

    /**
     * Sets the root of the node.
     *
     * @param NodeInterface $root The NodeInterface instance.
     */
    public function setRoot(NodeInterface $root);

    /**
     * Traverses current tree.
     *
     * @return \Traversable The Traversable instance.
     */
    public function all(): \Traversable;

    /**
     * Exports current tree into something.
     *
     * @return mixed The result of export.
     */
    public function export();
}
