<?php

namespace App\DataStructure\Tree\Interfaces;

use App\DataStructure\Tree\Interfaces\NodeInterface;

/**
 * NodeInterface.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
interface NodeInterface
{
    /**
     * Constructor.
     *
     * @param miexd $value The node value.
     */
    public function __construct($value);

    /**
     * Gets the value of the node.
     *
     * @return mixed The value of the node.
     */
    public function getValue();

    /**
     * Sets the value to the node.
     *
     * @param mixed $value The value of the node.
     */
    public function setValue($value);

    /**
     * Gets the name of the node.
     *
     * @return string The name of the node.
     */
    public function getName(): ?string;

    /**
     * Sets the name of the node.
     *
     * @param string $name The name of the node.
     */
    public function setName(?string $name);

    /**
     * Adds the child node to the current node.
     *
     * @param NodeInterface $child The child node.
     */
    public function addChild(NodeInterface $child);

    /**
     * Returns the collection of child nodes.
     *
     * @return array The collection of child nodes.
     */
    public function children(): array;

    /**
     * Counts child nodes.
     *
     * @return int The number of child nodes.
     */
    public function countChildren(): int;
}
