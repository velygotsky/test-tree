<?php

namespace App\DataStructure\Tree;

use App\DataStructure\Tree\Interfaces\NodeInterface;

/**
 * TreeNode class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
class TreeNode implements NodeInterface
{
    /**
     * @var mixed
     */
    private $value;

    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $children;

    /**
     * {@inheritdoc}
     */
    public function __construct($name = null, $value = null)
    {
        $this->name = $name;
        $this->value = $value;
        $this->children = [];
    }

    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function setName(?string $name)
    {
        $this->name = $name;
    }

    /**
     * {@inheritdoc}
     */
    public function addChild(NodeInterface $child)
    {
        $this->children[] = $child;
    }

    /**
     * {@inheritdoc}
     */
    public function children(): array
    {
        return $this->children;
    }

    /**
     * {@inheritdoc}
     */
    public function countChildren(): int
    {
        return count($this->children);
    }
}
