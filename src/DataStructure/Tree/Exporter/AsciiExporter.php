<?php

namespace App\DataStructure\Tree\Exporter;

use App\DataStructure\Tree\Interfaces\NodeInterface;
use App\DataStructure\Tree\Interfaces\ExporterInterface;

/**
 * AsciiExporter class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
class AsciiExporter implements ExporterInterface
{
    /**
     * {@inheritdoc}
     */
    public function export(NodeInterface $node): string
    {
        $tree = new \RecursiveTreeIterator(
            new \RecursiveArrayIterator(
                $this->toArray($node)
            ),
            \RecursiveTreeIterator::SELF_FIRST,
            \CachingIterator::CATCH_GET_CHILD,
            \RecursiveTreeIterator::SELF_FIRST
        );

        $tree->setPrefixPart(\RecursiveTreeIterator::PREFIX_LEFT, '');
        $tree->setPrefixPart(\RecursiveTreeIterator::PREFIX_END_HAS_NEXT, '├─');
        $tree->setPrefixPart(\RecursiveTreeIterator::PREFIX_END_LAST, '└─');
        $tree->setPrefixPart(\RecursiveTreeIterator::PREFIX_RIGHT, '');
        $tree->setPrefixPart(\RecursiveTreeIterator::PREFIX_MID_LAST, '  ');
        $tree->setPrefixPart(\RecursiveTreeIterator::PREFIX_MID_HAS_NEXT, '│ ');

        $output = '';

        foreach ($tree as $value) {
            $entry = $tree->getEntry();
            $output .= sprintf(
                '%s%s%s%s',
                $tree->getPrefix(),
                'Array' === $entry ? '┐' : ' ' . $entry,
                $tree->getPostfix(),
                \PHP_EOL
            );
        }

        return $output;
    }

    /**
     * Exports the tree in an array.
     *
     * @param NodeInterface $node The node.
     *
     * @return array The tree exported into an array.
     */
    private function toArray(NodeInterface $node): array
    {
        $children = [];
        foreach ($node->children() as $child) {
            $children[] = $this->toArray($child);
        }

        return count($children) == 0 ?
            [$this->getNodeLabel($node)] :
            [$this->getNodeLabel($node), $children];
    }

    /**
     * Get a string representation of the node.
     *
     * @param NodeInterface $node The node.
     *
     * @return string The node representation.
     */
    private function getNodeLabel(NodeInterface $node): string
    {
        if ($node->getName() !== null && $node->getValue() !== null) {
            return sprintf('%s %s', $node->getName(), $node->getValue());
        }

        return '' . ($node->getName() ?? $node->getValue());
    }
}
