<?php

namespace App\DataStructure\Tree;

use App\Algorithm\Traverser\Interfaces\TraverserInterface;
use App\Algorithm\Traverser\BreadthFirstTraverser;
use App\DataStructure\Tree\Interfaces\ExporterInterface;
use App\DataStructure\Tree\Interfaces\TreeInterface;
use App\DataStructure\Tree\Interfaces\NodeInterface;
use App\DataStructure\Tree\TreeNode;
use App\DataStructure\Tree\Exporter\AsciiExporter;

/**
 * GenericTree class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
class GenericTree implements TreeInterface
{
    /**
     * @var TraverserInterface
     */
    private $traverser;

    /**
     * @var ExporterInterface
     */
    private $exporter;

    /**
     * @var NodeInterface
     */
    private $root;

    /**
     * {@inheritdoc}
     */
    public function __construct(TraverserInterface $traverser = null, ExporterInterface $exporter = null)
    {
        $this->traverser = $traverser ?? new BreadthFirstTraverser();
        $this->exporter = $exporter ?? new AsciiExporter();
    }

    /**
     * {@inheritdoc}
     */
    public function all(): \Traversable
    {
        return $this->traverser->traverse($this->root);
    }

    /**
     * {@inheritdoc}
     */
    public function getRoot(): ?NodeInterface
    {
        return $this->root;
    }

    /**
     * {@inheritdoc}
     */
    public function setRoot(NodeInterface $root)
    {
        $this->root = $root;
    }

    /**
     * {@inheritdoc}
     */
    public function export()
    {
        return $this->exporter->export($this->root);
    }

    /**
     * Inserts the data from array in the current tree.
     *
     * @param array $array The array with values.
     *
     * @return TreeNode The TreeNode instance.
     */
    public function fromArray(array $array)
    {
        $node = new TreeNode();
        if ($this->root === null) {
            $this->root = $node;
        }
        if (isset($array['value'])) {
            $node->setValue($array['value']);
        }
        if (isset($array['name'])) {
            $node->setName($array['name']);
        }

        if (isset($array['children'])) {
            foreach ($array['children'] as $arrayChild) {
                $node->addChild($this->fromArray($arrayChild));
            }
        }

        return $node;
    }
}
