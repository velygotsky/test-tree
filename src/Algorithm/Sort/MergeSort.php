<?php

namespace App\Algorithm\Sort;

use App\DataStructure\LinkedList\SinglyLinkedList;
use App\DataStructure\LinkedList\Interfaces\ListInterface;

/**
 * MergeSort class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
class MergeSort
{
    /**
     * Sorts the linked list ascending.
     *
     * @param ListInterface $list The linked list to sort.
     *
     * @return ListInterface The linked list sorted ascending.
     */
    public function sort(ListInterface $list): ListInterface
    {
        if ($list->count() <= 1) {
            return $list;
        }

        // Find the middle point to divide the linked list into two halves.
        $middle = (int) floor($list->count() / 2);

        // Merge the two sorted halves.
        return $this->merge(
            // Recursion call for the left half.
            $this->sort($list->sublist(0, $middle)),
            // Recursion call for the right half.
            $this->sort($list->sublist($middle))
        );
    }

    /**
     * Merges two linked lists into one.
     *
     * @param ListInterface $left  The left half of the linked list.
     * @param ListInterface $right The right half of the linked list.
     *
     * @return ListInterface The linked list merged from two halves.
     */
    private function merge(ListInterface $left, ListInterface $right): ListInterface
    {
        $result = new SinglyLinkedList();
        while ($left->count() != 0 && $right->count() != 0) {
            // Rewind the iteretor at the begining.
            $left->rewind();
            $right->rewind();

            // Pick the smallest one first.
            if ($left->current() > $right->current()) {
                $result->push($right->shift());
            } else {
                $result->push($left->shift());
            }
        }

        // Add the rest values from the left half.
        while ($left->count() != 0) {
            $result->push($left->shift());
        }

        // Add the rest values from the right half.
        while ($right->count() != 0) {
            $result->push($right->shift());
        }

        return $result;
    }
}
