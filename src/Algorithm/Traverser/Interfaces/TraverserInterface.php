<?php

namespace App\Algorithm\Traverser\Interfaces;

use App\DataStructure\Tree\Interfaces\NodeInterface;

/**
 * TraverserInterface.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
interface TraverserInterface
{
    /**
     * Traverses the tree.
     *
     * @param NodeInterface $node The root of the tree.
     *
     * @return \Traversable The Traversable instance.
     */
    public function traverse(NodeInterface $node): \Traversable;
}
