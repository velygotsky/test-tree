<?php

namespace App\Algorithm\Traverser;

use App\Algorithm\Traverser\Interfaces\TraverserInterface;
use App\DataStructure\Tree\Interfaces\NodeInterface;

/**
 * BreadthFirstTraverser class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
class BreadthFirstTraverser implements TraverserInterface
{
    /**
     * {@inheritdoc}
     */
    public function traverse(NodeInterface $node): \Traversable
    {
        $queue = [];
        $queue[] = $node;
        while (count($queue) > 0) {
            $node = array_shift($queue);

            yield $node;

            foreach ($node->children()  as $child) {
                $queue[] = $child;
            }
        }
    }
}
