<?php

namespace App\Algorithm\Traverser;

use App\Algorithm\Traverser\Interfaces\TraverserInterface;
use App\DataStructure\Tree\Interfaces\NodeInterface;

/**
 * DepthFirstTraverser class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
class DepthFirstTraverser implements TraverserInterface
{
    /**
     * {@inheritdoc}
     */
    public function traverse(NodeInterface $node): \Traversable
    {
        $stack = [];
        $stack[] = $node;
        while (count($stack) > 0) {
            $node = array_pop($stack);

            yield $node;

            // Last child is pushed first, so that first child is processed first.
            foreach (array_reverse($node->children()) as $child) {
                $stack[] = $child;
            }
        }
    }
}
