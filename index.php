<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/vendor/autoload.php';


use App\Algorithm\Traverser\BreadthFirstTraverser;
use App\Algorithm\Traverser\DepthFirstTraverser;
use App\DataStructure\LinkedList\SinglyLinkedList;
use App\TaskTree;

const MAX_WEIGHT = 3;

if ('cli' != php_sapi_name()) {
    echo '<pre>', PHP_EOL;
}

$arrLetters = [
    'name' => 'A0',
    'value' => SinglyLinkedList::fromArray([2, 0, 1, 3, 1]),
    'children' => [
        0 => [
            'name' => 'A1',
            'value' => SinglyLinkedList::fromArray([2, 1]),
            'children' => [
                0 => [
                    'name' => 'A11'
                ],
            ]
        ],
        1 => [
            'name' => 'A2',
            'value' => SinglyLinkedList::fromArray([4]),
        ],
        2 => [
            'name' => 'A3',
        ],
    ],
];

$tree1 = new TaskTree(new BreadthFirstTraverser());
$tree1->fromArray($arrLetters);
echo 'Initial tree.', PHP_EOL;
echo $tree1->export(), PHP_EOL, PHP_EOL;

echo 'Reorganize with Breadth First algorithm of traversing.', PHP_EOL;
$tree1->reorganize(MAX_WEIGHT);
echo $tree1->export(), PHP_EOL, PHP_EOL;

$tree2 = new TaskTree(new DepthFirstTraverser());
$tree2->fromArray($arrLetters);

echo 'Reorganize with Depth First algorithm of traversing.', PHP_EOL;
$tree2->reorganize(MAX_WEIGHT);
echo $tree2->export(), PHP_EOL, PHP_EOL;