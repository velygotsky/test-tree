<?php

namespace App\Tests\Algorithm\Traverser;

use App\Algorithm\Traverser\BreadthFirstTraverser;
use App\DataStructure\Tree\GenericTree;
use PHPUnit\Framework\TestCase;

/**
 * BreadthFirstTraverserTest class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
final class BreadthFirstTraverserTest extends TestCase
{
    public function testTravers()
    {
        $array = [
            'value' => 'F',
            'children' => [
                [
                    'value' => 'B',
                    'children' => [
                        [
                            'value' => 'A',
                        ],
                        [
                            'value' => 'D',
                            'children' => [
                                [
                                    'value' => 'C',
                                ],
                                [
                                    'value' => 'E',
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    'value' => 'G',
                    'children' => [
                        [
                            'value' => 'I',
                            'children' => [
                                [
                                    'value' => 'H',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $tree = new GenericTree(new BreadthFirstTraverser());
        $tree->fromArray($array);
        $expected = ['F', 'B', 'G', 'A', 'D', 'I', 'C', 'E', 'H'];
        $actual = [];
        foreach ($tree->all() as $node) {
            $actual[] = $node->getValue();
        }

        $this->assertTrue($expected === $actual);
    }
}
