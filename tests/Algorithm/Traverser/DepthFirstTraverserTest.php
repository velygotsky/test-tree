<?php

namespace App\Tests\Algorithm\Traverser;

use App\Algorithm\Traverser\DepthFirstTraverser;
use App\DataStructure\Tree\GenericTree;
use PHPUnit\Framework\TestCase;

/**
 * DepthFirstTraverserTest class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
final class DepthFirstTraverserTest extends TestCase
{
    public function testTravers()
    {
        $array = [
            'value' => 'F',
            'children' => [
                [
                    'value' => 'B',
                    'children' => [
                        [
                            'value' => 'A',
                        ],
                        [
                            'value' => 'D',
                            'children' => [
                                [
                                    'value' => 'C',
                                ],
                                [
                                    'value' => 'E',
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    'value' => 'G',
                    'children' => [
                        [
                            'value' => 'I',
                            'children' => [
                                [
                                    'value' => 'H',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        $tree = new GenericTree(new DepthFirstTraverser());
        $tree->fromArray($array);
        $expected = ['F', 'B', 'A', 'D', 'C', 'E', 'G', 'I', 'H'];
        $actual = [];
        foreach ($tree->all() as $node) {
            $actual[] = $node->getValue();
        }

        $this->assertTrue($expected === $actual);
    }
}
