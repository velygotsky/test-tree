<?php

namespace App\Tests\Algorithm\Sort;

use App\Algorithm\Sort\MergeSort;
use App\DataStructure\LinkedList\SinglyLinkedList;
use PHPUnit\Framework\TestCase;

/**
 * MergeSortTest class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
final class MergeSortTest extends TestCase
{
    public function testSort()
    {
        // Generate array of random integers and shuffle it.
        $values = range(1, 20);
        $ascending = $values;
        shuffle($values);

        // Create singly linked list.
        $list = new SinglyLinkedList();
        foreach ($values as $value) {
            $list->push($value);
        }

        $mergeSort = new MergeSort();
        $sorted = $mergeSort->sort($list);

        // Case 1: Check size.
        $this->assertEquals($list->count(), $sorted->count());

        // Case 2: Check order.
        foreach ($sorted as $key => $value) {
            $this->assertEquals($ascending[$key], $value);
        }
    }
}
