<?php

namespace App\Tests\DataStructure\LinkedList;

use App\DataStructure\LinkedList\SinglyLinkedList;
use PHPUnit\Framework\TestCase;

/**
 * SinglyLinkedListTest class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
final class SinglyLinkedListTest extends TestCase
{
    public function testPush()
    {
        $values = [1, 2, 3, 4, 5];
        $list = SinglyLinkedList::fromArray($values);

        // Case 1: Check size.
        $this->assertEquals(count($values), $list->count());

        // Case 2: Check order.
        foreach ($list as $key => $value) {
            $this->assertEquals($values[$key], $value);
        }
    }

    public function testUnshift()
    {
        $list = new SinglyLinkedList();

        $values = [1, 2, 3, 4, 5];
        foreach (array_reverse($values) as $value) {
            $list->unshift($value);
        }

        // Case 1: Check size.
        $this->assertEquals(count($values), $list->count());

        // Case 2: Check order.
        foreach ($list as $key => $value) {
            $this->assertEquals($values[$key], $value);
        }
    }

    public function testShift()
    {
        $values = [1, 2, 3, 4, 5];
        $list = SinglyLinkedList::fromArray($values);

        // Case 1: Check shifted values.
        while ($list->count() > 0) {
            $this->assertEquals(array_shift($values), $list->shift());
        }

        // Case 2: Check size.
        $this->assertEquals(0, $list->count());
    }

    public function testPop()
    {
        $values = [1, 2, 3, 4, 5];
        $list = SinglyLinkedList::fromArray($values);

        // Case 1: Check popped values.
        while ($list->count() > 0) {
            $this->assertEquals(array_pop($values), $list->pop());
        }

        // Case 2: Check size.
        $this->assertEquals(0, $list->count());
    }

    public function testSublist()
    {
        $values = [1, 2, 3, 4, 5];
        $list = SinglyLinkedList::fromArray($values);

        // Case 1: Negative offset or size.
        $this->assertEquals(0, $list->sublist(-1, 3)->count());
        $this->assertEquals(0, $list->sublist(0, -3)->count());

        $expected = $actual = [];
        // Case 2: Left sub-list.
        $expected[] = array_slice($values, 0, 3);
        $actual[] = $list->sublist(0, 3);

        // Case 3: Right sub-list.
        $expected[] = array_slice($values, 3);
        $actual[] = $list->sublist(3);

        // Case 4: Middle sub-list.
        $expected[] = array_slice($values, 1, 3);
        $actual[] = $list->sublist(1, 3);


        foreach ($expected as $caseId => $case) {
            // Check size.
            $this->assertEquals(count($case), $actual[$caseId]->count());

            // Check order.
            foreach ($actual[$caseId] as $key => $value) {
                $this->assertEquals($value, $case[$key]);
            }
        }
    }

    public function testKeyRewind()
    {
        $list = new SinglyLinkedList();

        // Case 1: Initial key is 0.
        $this->assertEquals(0, $list->key());

        $values = [1, 2, 3, 4, 5];
        foreach ($values as $value) {
            $list->push($value);
        }

        // Case 2: After pushing values the key is 0.
        $this->assertEquals(0, $list->key());

        // Case 3: After loop the key is equal count.
        foreach ($list as $value) {}
        $this->assertTrue($list->key() == $list->count());

        // Case 4: After rewind the key is equal count.
        $list->rewind();
        $this->assertEquals(0, $list->key());
    }

    public function testCurrentNext()
    {
        $values = [1, 2, 3, 4, 5];
        $list = SinglyLinkedList::fromArray($values);

        $list->rewind();
        while ($list->valid()) {
            $this->assertEquals($values[$list->key()], $list->current());
            $list->next();
        }
    }
}
