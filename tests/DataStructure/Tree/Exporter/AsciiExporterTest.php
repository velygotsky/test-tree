<?php

namespace App\Tests\DataStructure\Tree\Exporter;

use App\DataStructure\Tree\GenericTree;
use App\DataStructure\Tree\Exporter\AsciiExporter;
use PHPUnit\Framework\TestCase;

const TREE_ARRAY = [
    'value' => 'F',
    'children' => [
        [
            'value' => 'B',
            'children' => [
                [
                    'value' => 'A',
                ],
                [
                    'value' => 'D',
                    'children' => [
                        [
                            'value' => 'C',
                        ],
                        [
                            'value' => 'E',
                        ],
                    ],
                ],
            ],
        ],
        [
            'value' => 'G',
            'children' => [
                [
                    'value' => 'I',
                    'children' => [
                        [
                            'value' => 'H',
                        ],
                    ],
                ],
            ],
        ],
    ],
];

const TREE_ASCII = <<<EOT
├─ F
└─┐
  ├─┐
  │ ├─ B
  │ └─┐
  │   ├─┐
  │   │ └─ A
  │   └─┐
  │     ├─ D
  │     └─┐
  │       ├─┐
  │       │ └─ C
  │       └─┐
  │         └─ E
  └─┐
    ├─ G
    └─┐
      └─┐
        ├─ I
        └─┐
          └─┐
            └─ H

EOT;

/**
 * AsciiExporterTest class.
 *
 * @author Yaroslav Velygotsky <yaroslav@velygotsky.com>
 */
final class AsciiExporterTest extends TestCase
{
    public function testExport()
    {
        $tree = new GenericTree(null, new AsciiExporter());
        $tree->fromArray(TREE_ARRAY);
        $actual = $tree->export();
        $this->assertEquals(TREE_ASCII, $actual);
    }
}
