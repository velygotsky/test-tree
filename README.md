# Task

#### Given:
* Tree of arbitrary depth;
* Tree nodes have leaves (0 - N);
* Each leaf has an associated weight — a non-zero integer;
* Leaves of each node are presented as a singly linked list.

#### Conditions:
* For each node — the script should sort the leaves by weight without using library sorting functions,
* while the sum of the leaf weights for each specific tree node should not exceed a given constant W,
* excess leaves are transferred to the next tree node (in accordance with the tree node traversal algorithm),
* excess leaves of the very last node of the tree are discarded.

#### Example:
A tree node has 3 child nodes (a1, a2, a3) as well as 4 leaves (b1, b2, b3, b4) with resp. weights: 1 for b1, 2 for b2, 3 for b3 and 4 for b4

Constant W = 3

The initial state of the leaf list for this node: b2, b4, b3, b1

The final state of the leaf list for this node: b1, b2, while the leaves b3, b4 are transferred to the child node a1.

#### Other:
* Project on BitBucket or GitHub;
* Demonstration of using GitFlow workflow;
* Test coverage.

## Installation

Use the [composer](https://getcomposer.org/download/) package manager to install dependencies.

```bash
php composer.phar install
```

## Usage

Run the `index.php` script from the CLI:

```
php index.php
```
It will show you the result.

```
Initial tree.
├─ A0 [2, 0, 1, 3, 1]
└─┐
  ├─┐
  │ ├─ A1 [2, 1]
  │ └─┐
  │   └─┐
  │     └─ A11
  ├─┐
  │ └─ A2 [4]
  └─┐
    └─ A3


Reorganize with Breadth First algorithm of traversing.
├─ A0 [0, 1, 1]
└─┐
  ├─┐
  │ ├─ A1 [1, 2]
  │ └─┐
  │   └─┐
  │     └─ A11
  ├─┐
  │ └─ A2 [2]
  └─┐
    └─ A3 [3]


Reorganize with Depth First algorithm of traversing.
├─ A0 [0, 1, 1]
└─┐
  ├─┐
  │ ├─ A1 [1, 2]
  │ └─┐
  │   └─┐
  │     └─ A11 [2]
  ├─┐
  │ └─ A2 [3]
  └─┐
    └─ A3

```
## How to run tests?
```bash
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/
```